# config_repo

#### 项目介绍
spring cloud config 配置中心资源仓库

#### 配置资源说明
> fw-test.properties TestProject服务配置资源
> fw-service-provider.properties SpringCloudServiceProvider服务配置资源
> fw-service-customer.properties SpringCloudServiceCustomer服务配置资源
